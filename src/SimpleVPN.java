import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.lang.reflect.Array;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

/**
 * Created by William Hsiao on 2015/10/18.
 */
public class SimpleVPN {
    // GUI
    private JFrame frame;
    private JLabel hostLabel = new JLabel("Enter host IP");
    private JLabel portLabel = new JLabel("Pick a port");
    private JLabel keyLabel = new JLabel("Enter a a shared key");
    private JTextField hostIPTextField = new JTextField();
    private JTextField hostPortTextField = new JTextField();;
    private JTextField sharedKeyTextField = new JTextField();;
    private JButton serverButton = new JButton("Server mode");
    private JButton clientButton = new JButton("Client mode");

    public static void main (String args[])
    {
        SimpleVPN vpn = new SimpleVPN();
    }

    public SimpleVPN()
    {
        renderGUI();
    }
    private void renderGUI()
    {
        JFrame frame = new JFrame("simpleVPN");
        frame.setResizable(false);
        frame.setLayout(new GridLayout(5, 2));
        frame.add(new JLabel("Enter infos and pick a mode"));
        frame.add(new JLabel());
        frame.add(hostLabel);
        frame.add(hostIPTextField);
        frame.add(portLabel);
        frame.add(hostPortTextField);
        frame.add(keyLabel);
        frame.add(sharedKeyTextField);
        frame.add(serverButton);
        frame.add(clientButton);
        serverButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!checkReq()){
                    JOptionPane.showMessageDialog(null, "Please enter all the required information");
                    return;
                }
                frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
                Server s = new Server(hostPortTextField.getText(), sharedKeyTextField.getText());
            }
        });
        clientButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!checkReq()){
                    JOptionPane.showMessageDialog(null, "Please enter all the required information");
                    return;
                }
                frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
                Client c = new Client(hostIPTextField.getText(),
                                    hostPortTextField.getText(),
                                    sharedKeyTextField.getText());
            }
        });
        frame.pack();
        frame.setVisible(true);

    }
    private boolean checkReq(){
        return (hostIPTextField.getText() != null && hostIPTextField.getText().length() > 0 &&
                hostPortTextField.getText() != null && hostPortTextField.getText().length() > 0 &&
                sharedKeyTextField.getText() != null && sharedKeyTextField.getText().length() >0);
    }

}
