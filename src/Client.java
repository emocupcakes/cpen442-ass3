import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.Random;
import java.security.*;
import java.security.spec.*;
import javax.crypto.*;
import javax.crypto.spec.*;


/**
 * Created by William Hsiao on 2015/10/12.
 */
public class Client {
    private static String SERVER_NAME = "442Server";
    private static String CLIENT_NAME = "442Client";
    private Socket serverSocket;
    private DataOutputStream outputStream;
    private DataInputStream inputStream;
    private String SHARED_KEY;

    // Mutual Authentication
    private byte[] myNonce;
    private byte[] serverNonce;
    private boolean authorized = false;
    private KeyAgreement clientKeyAgree;// = KeyAgreement.getInstance("DH");
    private byte[] sessionKey;

    // GUI
    private JFrame frame;
    private JScrollPane chatHistoryContainer;
    private JTextArea chatHistory;
    private JLabel label;
    private JTextField inputField;
    private JButton send;

    /* *
     * Constructor for Client
     * @param String host - host name e.g. localhost or 192.168.0.7
     * @param String port - port number to connect to on server
     * @param String sharedKey - shared symmetric key
     */
    public Client(String host, String port, String sharedKey) {
        // Initialization section:
        // Try to open input and output streams
        if (host == null || port == null || sharedKey == null){
            JOptionPane.showMessageDialog(null, "something went wrong");
            return;
        }
        SHARED_KEY = sharedKey;
        renderGUI();
        try {
            serverSocket = new Socket(host, Integer.valueOf(port));
            outputStream = new DataOutputStream(serverSocket.getOutputStream());
            inputStream = new DataInputStream(serverSocket.getInputStream());
        } catch (UnknownHostException e) {
            System.err.println("Don't know about host: hostname");
        } catch (IOException e) {
            System.err.println("Couldn't get I/O for the connection to: hostname");
        }

        // If everything has been initialized then we want to write some data
        if (serverSocket != null && outputStream != null && inputStream != null) {
        	try {
				mutualAuthentication();
                startChat();
			} catch (Exception e) {
				System.err.println("Nonce problem: " + e);
			}
        }
    }

    /* * *
     * initiates chat session with connected Client
     */
    private void startChat()
    {
        while(true)
        {
            try{
                byte[] encryptedInput = readBytes();
                byte[] decryptedInput = decrypt(encryptedInput, SHARED_KEY, sessionKey);
                String inputString = new String(decryptedInput);
                toChatHistory("Received cipher text: " + toHexString(encryptedInput));
                toChatHistory("Server: " + inputString);
            }
            catch (Exception e)
            {
                System.err.println(e.getMessage());
            }


        }
    }

    /* * *
     * write chat history to log
     * @param String msg - the message between client and server
     */
    private void toChatHistory(String msg)
    {
        chatHistory.append(msg + "\n");
        int height = (int)chatHistory.getSize().getHeight();
        chatHistoryContainer.getVerticalScrollBar().setValue(height);
    }


    /* *
     * Draw UI
     */
    private void debug(String msg)
    {
        toChatHistory("======DEBUG======");
        toChatHistory(msg);
        toChatHistory("====DEBUG-END====");
    }

    private void renderGUI()
    {
        JFrame frame = new JFrame("Client");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Set up the content pane.
        addComponentsToPane(frame.getContentPane());

        //Display the window.
        frame.pack();
        frame.setVisible(true);
    }
    
    /* *
     * UI utility function
     */
    public void addComponentsToPane(Container pane) {
        pane.setLayout(new BoxLayout(pane, BoxLayout.Y_AXIS));

        chatHistoryContainer = new JScrollPane(20,30);
        chatHistoryContainer.setAlignmentX(Component.LEFT_ALIGNMENT);
        chatHistory = new JTextArea(20,30);
        chatHistory.setAlignmentX(Component.LEFT_ALIGNMENT);
        chatHistory.setLineWrap(true);
        chatHistoryContainer = new JScrollPane(chatHistory);
        chatHistoryContainer.setAlignmentX(Component.LEFT_ALIGNMENT);
        pane.add(chatHistoryContainer);


        label = new JLabel("input:");
        label.setAlignmentX(Component.LEFT_ALIGNMENT);
        pane.add(label);

        inputField = new JTextField();
        inputField.setAlignmentX(Component.LEFT_ALIGNMENT);
        pane.add(inputField);

        send = new JButton("send");
        send.setAlignmentX(Component.LEFT_ALIGNMENT);
        send.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String input = inputField.getText();
                toChatHistory("Client: " + input);
                byte[] encryptedInput = encrypt(input, SHARED_KEY, sessionKey);
                toChatHistory("Sending cipher text: " + toHexString(encryptedInput));
                sendBytes(encryptedInput);
                inputField.setText("");
            }
        });
        pane.add(send);
    }

    /* 
     * Mutual Authentication to protect against Man in the Middle Attack - BAD NEWS for Trudy (aka dirty skank)
     */
    private void mutualAuthentication() throws Exception {
        chatHistory.append("Begin Mutual Authentication base on Shared key \n");
        myNonce = diffieHClient();
        chatHistory.append("Nonce: " + myNonce + "\n");

        // start convo, sendByte screws up the nonce if not sending as string.getByte
        sendBytes("HELO".getBytes());
        sendBytes(CLIENT_NAME.getBytes());
        sendBytes(myNonce);

        String message = "";
        byte[] messageByte;
        while(!authorized){
            try {
                messageByte = readBytes();
                message = new String(messageByte);
                switch (message){
                	//Determine if receiver is the correct recipient 
                    case "PROF":
                        chatHistory.append("Get PROF request \n");
                        byte[] nonce = readBytes();
                        byte[] received = readBytes();
                        toChatHistory("Server nonce: " + toHexString(nonce));
                        toChatHistory("Server authentification message: " + toHexString(received));
                        byte[] expected = encrypt(SERVER_NAME, SHARED_KEY, myNonce);
                        if (Arrays.equals(expected,received))
                        {
                            chatHistory.append("Expected recipient confirmed \n");
                            serverNonce = nonce;
                            byte[] encrypted = encrypt(CLIENT_NAME, SHARED_KEY, serverNonce);
                            sendBytes("AUTH".getBytes());
                            sendBytes(encrypted);
                        }
                        break;
                    //Recipient confirmed, authorized to send/retreive messages from    
                    case "AUTH":
                    	secretKey(serverNonce);
                        authorized = true;
                        break;
                    default:
                        sendBytes("HELO".getBytes());
                        sendBytes(CLIENT_NAME.getBytes());
                        sendBytes(myNonce); 
                        break;
                }
            }
            catch (SocketTimeoutException e){
                sendBytes("HELO".getBytes());
                sendBytes(CLIENT_NAME.getBytes());
                sendBytes(myNonce);
            }
            catch (Exception e){
                System.err.println("Exception:  " + e);
                break;
            }
        }
        chatHistory.append("done mutual authentication \n");
    }

    /**
     *@param  plainText text to encrypt
     * @param key key to encrypt with
     * @param myNonce2 seed use for encryption
     * @return Encrypted byte[] of plainText
     */
    private byte[] encrypt(String plainText, String key, byte[] myNonce2)
    {
        try{
            byte[] plainTextBytes = addPadding(plainText.getBytes());
            byte[] keyBytes = generateSHA(key.getBytes(), "", 16);
            byte[] seedBytes = generateSHA(myNonce2, "", 16);

            SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");
            IvParameterSpec seedSpec = new IvParameterSpec(seedBytes);

            Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
            cipher.init(Cipher.ENCRYPT_MODE, keySpec, seedSpec);
            byte[] result = new byte[cipher.getOutputSize(plainTextBytes.length)];
            int enc_len = cipher.update(plainTextBytes, 0, plainTextBytes.length, result, 0);
            enc_len += cipher.doFinal(result, enc_len);
            //toChatHistory("Cipher text: " + toHexString(result));
            return result;
        }catch (Exception e){
            System.err.println(e);
        }
        return null;
    }

    /**
     *@param  cipherBytes text to decrypt
     * @param key key to decrypt with
     * @param seed seed use for decryption
     * @return decrypted byte[] of cipher
     */
    private byte[] decrypt(byte[] cipherBytes, String key, byte[] seed)
    {
        try{
            byte[] cipherBytesWithPad = addPadding(cipherBytes);
            byte[] keyBytes = generateSHA(key.getBytes(), "", 16);
            byte[] seedBytes = generateSHA(seed, "", 16);

            SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");
            IvParameterSpec seedSpec = new IvParameterSpec(seedBytes);

            Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
            cipher.init(Cipher.DECRYPT_MODE, keySpec, seedSpec);
            byte[] result = new byte[cipher.getOutputSize(cipherBytesWithPad.length)];
            int dec_len = cipher.update(cipherBytesWithPad, 0, cipherBytesWithPad.length, result, 0);
            dec_len += cipher.doFinal(result, dec_len);
            return result;
        }catch (Exception e){
            System.err.println(e);
        }
        return null;
    }

    /*
     * generates SHA hash
     * @param byte[] input - plain text to hash
     * @param String salt - salt to hash with
     * @param int outputLen - target length of output
     */
    private byte[] generateSHA(byte[] input, String salt, int outputLen)
    {
        try{
            byte[] saltByte = salt.getBytes();
            byte[] result = new byte[input.length + saltByte.length];
            System.arraycopy(input   ,0,result,0         ,input.length);
            System.arraycopy(saltByte, 0, result, input.length, saltByte.length);

            MessageDigest sha = MessageDigest.getInstance("SHA-1");
            result = sha.digest(result);
            result = Arrays.copyOf(result, outputLen);
            return result;
        }
        catch (Exception e)
        {
            System.err.println(e.getMessage());
        }

        return null;
    }
    
    /* * *
     * add padding to make byte array proper/accepted size
     * @param byte[] b 
     */
    private static byte[] addPadding(byte[] b)
    {
        if (b.length % 16 == 0)
            return b;
        int padCount = 16 - (b.length % 16);

        return Arrays.copyOf(b, b.length+padCount);
    }

    /* * *
     *  sending msg to Client in raw form
     *  @param byte[] bytes - raw data to send 
     */
    private void sendBytes(byte[] bytes)
    {
        if(this.outputStream == null)
        {
            return;
        }
        byte[] checksum = generateSHA(bytes, SHARED_KEY, 4);
        byte[] len = ByteBuffer.allocate(4).order(ByteOrder.BIG_ENDIAN).putInt(bytes.length).array();
//        toChatHistory("====DEBUG====");
//        toChatHistory("Checksum for message: " + toHexString(checksum));
        
        try
        {
            outputStream.write(checksum);
            outputStream.write(len);
            outputStream.write(bytes);
        }
        catch (Exception e)
        {
            System.err.println(e.getMessage());
        }
    }

    /* *
     * read msg sent by Client
     */
    private byte[] readBytes() throws java.io.IOException
    {
        if (inputStream == null)
        {
            return null;
        }
        byte[] checksum = new byte[4];
        if (inputStream.read(checksum) < 4)
            return null;
//        toChatHistory("====DEBUG====");
//        toChatHistory("Server message Checksum: " + toHexString(checksum));
        
        byte[] len = new byte[4];
        // read the first four bytes to get the length of the following msg
        if (inputStream.read(len) < 4)
            return null;

        ByteBuffer byteBuffer = ByteBuffer.wrap(len,0,4);
        int len_int = byteBuffer.getInt();
        byte[] buf = new byte[len_int];
        if (inputStream.read(buf) < len_int)
            return null;

        byte[] integrity = generateSHA(buf, SHARED_KEY, 4);

        if (!Arrays.equals(integrity, checksum))
        {
            JOptionPane.showMessageDialog(null, "something went wrong with the byte read/sent");
            System.exit(1);
        }
        return buf;
    }

    /*
     * @return byte[] client DH
     */
    private byte[] diffieHClient() throws Exception {

    	DHParameterSpec dhSkipParamSpec;  
    	
        AlgorithmParameterGenerator paramGen
        = AlgorithmParameterGenerator.getInstance("DH");
        paramGen.init(1024);
        AlgorithmParameters params = paramGen.generateParameters();
        dhSkipParamSpec = (DHParameterSpec)params.getParameterSpec
        (DHParameterSpec.class);
        
        //creating server DH
        KeyPairGenerator clientKpairGen = KeyPairGenerator.getInstance("DH");
        clientKpairGen.initialize(dhSkipParamSpec);
        KeyPair clientKpair = clientKpairGen.generateKeyPair();
        
        // Client creates and initializes her DH KeyAgreement object
        clientKeyAgree = KeyAgreement.getInstance("DH");
        clientKeyAgree.init(clientKpair.getPrivate());
        byte[] clientPubKeyEnc = clientKpair.getPublic().getEncoded();
        chatHistory.append("Client DH created and initialized \n");
        return clientPubKeyEnc;
    }
    
    /*
     * Creates and sets secret key
     */
    private void secretKey(byte[] clientPubKeyEnc) throws NoSuchAlgorithmException, InvalidKeySpecException, InvalidKeyException, IllegalStateException{

        KeyFactory clientKeyFac = KeyFactory.getInstance("DH");
        X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(clientPubKeyEnc);
        PublicKey clientPubKey = clientKeyFac.generatePublic(x509KeySpec);
        clientKeyAgree.doPhase(clientPubKey, true);
        
        byte[] clientSharedSecret = clientKeyAgree.generateSecret();

        toChatHistory("Session key: " + toHexString(clientSharedSecret) + "\n");
        sessionKey = clientSharedSecret;
    }
    
    /* *
     * Convert byte to hexadecimal
     */
    private void byte2hex(byte b, StringBuffer buf) {
        char[] hexChars = { '0', '1', '2', '3', '4', '5', '6', '7', '8',
            '9', 'A', 'B', 'C', 'D', 'E', 'F' };
        int high = ((b & 0xf0) >> 4);
        int low = (b & 0x0f);
        buf.append(hexChars[high]);
        buf.append(hexChars[low]);
    }
    
    /*
     * Converts a byte array to hex string
     */
    private String toHexString(byte[] block) {
        StringBuffer buf = new StringBuffer();
        
        int len = block.length;
        
        for (int i = 0; i < len; i++) {
            byte2hex(block[i], buf);
        }
        return buf.toString();
    }
 
    public static void main(String argv[]){
//        Client c = new Client("localhost", "9999", "shared_key");
        String host = argv[0];
        String port = argv[1];
        String key = argv[2];
        Client c = new Client(host, port, key);

    }
}
