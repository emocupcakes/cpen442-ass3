import javax.crypto.Cipher;
import javax.crypto.KeyAgreement;
import javax.crypto.interfaces.DHPublicKey;
import javax.crypto.spec.DHParameterSpec;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.swing.*;
import javax.swing.text.DefaultCaret;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;

/**
 * Created by William Hsiao on 2015/10/12.
 */

public class Server{
    private static String SERVER_NAME = "442Server";
    private static String CLIENT_NAME = "442Client";
    private String SHARED_KEY;

    private ServerSocket VPNServer;
    private DataInputStream inputStream;
    private DataOutputStream outputStream;
    private Socket clientSocket;

    // Mutual Authentication
    private byte[] myNonce;
    private byte[] clientNonce;
    private boolean authenticate = false;
    private KeyAgreement serverKeyAgree;
    private byte[] sessionKey;

    // GUI
    private JFrame frame;
    private JScrollPane chatHistoryContainer;
    private JTextArea chatHistory;
    private JLabel label;
    private JTextField inputField;
    private JButton send;

    /*
     * Server constructor
     * @param String port - port number
     * @param String sharedKey - the symmetric key used for encrypting and decrypting messages 
     */
    public Server(String port, String sharedKey) {
        if (port == null || sharedKey == null)
            return;
        this.SHARED_KEY = sharedKey;
        // Setup Server
        try {
            // Try to open a server socket on port that user specified
            VPNServer = new ServerSocket(Integer.valueOf(port));
            System.out.println("hereeeeeeee2");
            renderGUI();
        }
        catch (IOException e) {
            chatHistory.append(e.getMessage());
        }
        // Create a socket object from the ServerSocket to listen and accept
        try {
            chatHistory.append("waiting for client... \n");
            clientSocket = VPNServer.accept();
            inputStream = new DataInputStream(clientSocket.getInputStream());
            outputStream = new DataOutputStream(clientSocket.getOutputStream());
            chatHistory.append("Accepted connection and Setup In/OutputStream \n");
            mutualAuthentication();
            startChat();
        }
        catch (SocketTimeoutException e)
        {
            System.err.println("no information received from client");
        }
        catch (Exception e){
            System.err.println("Exception:  " + e);
        }
    }

    /* * *
     * initiates chat session with connected Client
     */
    private void startChat()
    {
        while(true)
        {
            try{
                byte[] encryptedInput = readBytes();
                toChatHistory("received cipher: " + toHexString(encryptedInput));
                byte[] decryptedInput = decrypt(encryptedInput, SHARED_KEY, sessionKey);
                String inputString = new String(decryptedInput);
                toChatHistory("Client: " + inputString);
            }
            catch (Exception e)
            {
                System.err.println(e.getMessage());
            }


        }
    }

    /* * *
     * write chat history to log
     * @param String msg - the message between client and server
     */
    private void toChatHistory(String msg)
    {
        chatHistory.append(msg + "\n");
        int height = (int)chatHistory.getSize().getHeight();
        chatHistoryContainer.getVerticalScrollBar().setValue(height);
    }

    /* *
     * Draw UI
     */
    private void renderGUI()
    {
        JFrame frame = new JFrame("Server");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Set up the content pane.
        addComponentsToPane(frame.getContentPane());

        //Display the window.
        frame.pack();
        frame.setVisible(true);
    }
    
    /* *
     * UI utility function
     */
    public void addComponentsToPane(Container pane) {
        pane.setLayout(new BoxLayout(pane, BoxLayout.Y_AXIS));

        chatHistoryContainer = new JScrollPane(20,30);
        chatHistoryContainer.setAlignmentX(Component.LEFT_ALIGNMENT);
        chatHistory = new JTextArea(20,30);
        chatHistory.setAlignmentX(Component.LEFT_ALIGNMENT);
        chatHistory.setLineWrap(true);
        chatHistoryContainer = new JScrollPane(chatHistory);
        chatHistoryContainer.setAlignmentX(Component.LEFT_ALIGNMENT);
        pane.add(chatHistoryContainer);

        label = new JLabel("input:");
        label.setAlignmentX(Component.LEFT_ALIGNMENT);
        pane.add(label);

        inputField = new JTextField();
        inputField.setAlignmentX(Component.LEFT_ALIGNMENT);
        pane.add(inputField);

        send = new JButton("send");
        send.setAlignmentX(Component.LEFT_ALIGNMENT);
        send.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String input = inputField.getText();
                toChatHistory("Server: " + input);
                byte[] encryptedInput = encrypt(input, SHARED_KEY, sessionKey);
                toChatHistory("sending cipher: " + encryptedInput);
                sendBytes(encryptedInput);
                inputField.setText("");
            }
        });
        pane.add(send);
    }

    /* 
     * Mutual Authentication to protect against Man in the Middle Attack - BAD NEWS for Trudy (aka dirty skank)
     */
    private void mutualAuthentication()
    {
        toChatHistory("Begin Mutual Authentication base on Shared key");

        while (!authenticate)
        {
            try{
                String message;
                byte[] messageByte;
                messageByte = readBytes();
                message = new String(messageByte);
                switch (message)
                {
                    case "HELO":
                        toChatHistory("Received HELO from Client");
                        String user = new String (readBytes());
                        byte[] nonce = readBytes();
                        myNonce = diffieHServer(nonce);
                        if (CLIENT_NAME.equals(user) && nonce != null)
                        {
                            clientNonce = nonce;
                            toChatHistory("User is: " + user);
                            toChatHistory("With Nonce " + toHexString(clientNonce));
                            sendBytes("PROF".getBytes());
                            sendBytes(myNonce);
                            byte[] encrypt = encrypt(SERVER_NAME, SHARED_KEY, clientNonce);
                            sendBytes(encrypt);
                            toChatHistory("Server Ask Client to Proof itself");
                            toChatHistory("With Server Nonce: " + toHexString(myNonce));
                        }
                        break;
                    case "AUTH":
                        toChatHistory("Received Proof from Client");
                        byte[] received = readBytes();
                        byte[] expected = encrypt(CLIENT_NAME, SHARED_KEY, myNonce);
                        toChatHistory("Expected : " + toHexString(expected));
                        toChatHistory("Received : " + toHexString(received));

                        if (Arrays.equals(received,expected))
                        {
                            toChatHistory("Proof Matched expected!");
                            sendBytes("AUTH".getBytes());
                            secretKey(clientNonce);
                            toChatHistory("generate session key with server nonce and client nonce");
                            authenticate = true;
                        }
                        break;
                }
            }catch(Exception e)
            {
                System.err.println(e.getMessage());
                break;
            }
        }

        toChatHistory("done mutual authentication");
    }

    /**
     *@param  plainText text to encrypt
     * @param key key to encrypt with
     * @param myNonce2 seed use for encryption
     * @return Encrypted byte[] of plainText
     */
    private byte[] encrypt(String plainText, String key, byte[] myNonce2)
    {
        try{
            byte[] plainTextBytes = addPadding(plainText.getBytes());
            byte[] keyBytes = generateSHA(key.getBytes(), "", 16);
            byte[] seedBytes = generateSHA(myNonce2, "", 16);

            SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");
            IvParameterSpec seedSpec = new IvParameterSpec(seedBytes);

            Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
            cipher.init(Cipher.ENCRYPT_MODE, keySpec, seedSpec);
            byte[] result = new byte[cipher.getOutputSize(plainTextBytes.length)];
            int enc_len = cipher.update(plainTextBytes, 0, plainTextBytes.length, result, 0);
            enc_len += cipher.doFinal(result, enc_len);
            return result;
        }catch (Exception e){
            chatHistory.append(e.getMessage());
        }
        return null;
    }

    /**
     *@param  cipherBytes text to decrypt
     * @param key key to decrypt with
     * @param seed seed use for decryption
     * @return decrypted byte[] of cipher
     */
    private byte[] decrypt(byte[] cipherBytes, String key, byte[] seed)
    {
        try{
            byte[] cipherBytesWithPad = addPadding(cipherBytes);
            byte[] keyBytes = generateSHA(key.getBytes(), "", 16);
            byte[] seedBytes = generateSHA(seed, "", 16);

            SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");
            IvParameterSpec seedSpec = new IvParameterSpec(seedBytes);

            Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
            cipher.init(Cipher.DECRYPT_MODE, keySpec, seedSpec);
            byte[] result = new byte[cipher.getOutputSize(cipherBytesWithPad.length)];
            int dec_len = cipher.update(cipherBytesWithPad, 0, cipherBytesWithPad.length, result, 0);
            dec_len += cipher.doFinal(result, dec_len);
            return result;
        }catch (Exception e){
            System.err.println(e);
        }
        return null;
    }

    /*
     * generates SHA hash
     * @param byte[] input - plain text to hash
     * @param String salt - salt to hash with
     * @param int outputLen - target length of output
     */
    private byte[] generateSHA(byte[] input, String salt, int outputLen)
    {
        try{
            byte[] saltByte = salt.getBytes();
            byte[] result = new byte[input.length + saltByte.length];
            System.arraycopy(input   ,0,result,0         ,input.length);
            System.arraycopy(saltByte, 0, result, input.length, saltByte.length);

            MessageDigest sha = MessageDigest.getInstance("SHA-1");
            result = sha.digest(result);
            result = Arrays.copyOf(result, outputLen);
            return result;
        }
        catch (Exception e)
        {
            System.err.println(e.getMessage());
        }

        return null;
    }
    
    /* * *
     * add padding to make byte array proper/accepted size
     * @param byte[] b 
     */
    private static byte[] addPadding(byte[] b)
    {
        if (b.length % 16 == 0)
            return b;
        int padCount = 16 - (b.length % 16);

        return Arrays.copyOf(b, b.length+padCount);
    }

    /* * *
     *  sending msg to Client in raw form
     *  @param byte[] bytes - raw data to send 
     */
    private void sendBytes(byte[] bytes)
    {
        if(this.outputStream == null)
        {
            return;
        }
        byte[] checksum = generateSHA(bytes, SHARED_KEY, 4);
        byte[] len = ByteBuffer.allocate(4).order(ByteOrder.BIG_ENDIAN).putInt(bytes.length).array();

        try
        {
            outputStream.write(checksum);
            outputStream.write(len);
            outputStream.write(bytes);
//            toChatHistory("======MSG DEBUG=====");
//            toChatHistory("checksum: " + toHexString(checksum));
//            toChatHistory("sent: " + toHexString(bytes));
//            toChatHistory("====MSG DEBUG END===");
        }
        catch (Exception e)
        {
            System.err.println(e.getMessage());
        }

    }

    /* *
     * read msg sent by Client
     */
    private byte[] readBytes() throws java.io.IOException
    {
        if (inputStream == null)
        {
            return null;
        }

        byte[] checksum = new byte[4];
        if (inputStream.read(checksum) < 4)
            return null;

        byte[] len = new byte[4];
        // read the first four bytes to get the length of the following msg
        if (inputStream.read(len) < 4)
            return null;

        ByteBuffer byteBuffer = ByteBuffer.wrap(len,0,4);
        int len_int = byteBuffer.getInt();
        byte[] buf = new byte[len_int];
        if (inputStream.read(buf) < len_int)
            return null;

        byte[] integrity = generateSHA(buf, SHARED_KEY, 4);

        if (!Arrays.equals(integrity, checksum))
        {
            JOptionPane.showMessageDialog(null, "something went wrong with the byte read/sent");
            System.exit(1);
        }

//        toChatHistory("======READ DEBUG=====");
//        toChatHistory("received: " + toHexString(buf));
//        toChatHistory("checksum: " + toHexString(checksum));
//        toChatHistory("expected checksum: " + toHexString(integrity));
//        toChatHistory("====READ DEBUG END===");
        return buf;
    }

    /**
     * @param  nonce of client public key
     * @return byte[] of server public key
     */
    private byte[] diffieHServer(byte[] nonce) throws Exception {

    	KeyFactory serverKeyFac = KeyFactory.getInstance("DH");
        X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec
        (nonce);
        PublicKey clientPubKey = serverKeyFac.generatePublic(x509KeySpec);
        
        DHParameterSpec dhParamSpec = ((DHPublicKey)clientPubKey).getParams();
        
        // Server DH key pair
        KeyPairGenerator serverKpairGen = KeyPairGenerator.getInstance("DH");
        serverKpairGen.initialize(dhParamSpec);
        KeyPair serverKpair = serverKpairGen.generateKeyPair();
        
        // Server initializes his DH KeyAgreement object
        serverKeyAgree = KeyAgreement.getInstance("DH");
        serverKeyAgree.init(serverKpair.getPrivate());
        
        // Server encodes his public key, and sends it over to Client
        byte[] serverPubKeyEnc = serverKpair.getPublic().getEncoded();
        return serverPubKeyEnc;
    }

    /*
     * Creates and sets secret key
     */
    private void secretKey(byte[] serverPubKeyEnc) throws NoSuchAlgorithmException, InvalidKeySpecException, InvalidKeyException, IllegalStateException{

        KeyFactory serverKeyFac = KeyFactory.getInstance("DH");
        X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(serverPubKeyEnc);
        PublicKey serverPubKey = serverKeyFac.generatePublic(x509KeySpec);
        serverKeyAgree.doPhase(serverPubKey, true);
        
        byte[] serverSharedSecret = serverKeyAgree.generateSecret();

        toChatHistory("session key" + toHexString(serverSharedSecret));
        sessionKey = serverSharedSecret;
    }
    
    /* *
     * Convert byte to hexadecimal
     */
    private void byte2hex(byte b, StringBuffer buf) {
        char[] hexChars = { '0', '1', '2', '3', '4', '5', '6', '7', '8',
            '9', 'A', 'B', 'C', 'D', 'E', 'F' };
        int high = ((b & 0xf0) >> 4);
        int low = (b & 0x0f);
        buf.append(hexChars[high]);
        buf.append(hexChars[low]);
    }
    
    /*
     * Converts a byte array to hex string
     */
    private String toHexString(byte[] block) {
        StringBuffer buf = new StringBuffer();
        
        int len = block.length;
        
        for (int i = 0; i < len; i++) {
            byte2hex(block[i], buf);
        }
        return buf.toString();
    }
    
    public static void main(String argv[]){
//        Server s = new Server("9999", "shared_key");
        String port = argv[0];
        String key = argv[1];
        Server s = new Server(port, key);
    }
}
